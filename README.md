# Drawing machines examples

Slides for the integration course with the students of Art & Tech at ErasmusHogeSchool, Brussels.

View the slides here: https://xuv.gitlab.io/drawing-machines-examples/

### How to use & modify

`git clone --recursive https://gitlab.com/xuv/drawing-machines-examples.git`
